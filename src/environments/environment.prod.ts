export const environment = {
  production: true,
  testUrl: 'http://localhost:3000',
  wikiUrl: 'https://en.wikipedia.org/w/api.php'
};
