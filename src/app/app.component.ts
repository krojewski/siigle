import { Component } from '@angular/core';
import { ResultsDataService } from './services/results-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: []
})
export class AppComponent {

  results: Object[] = [];
  
  constructor (private resultsDataService: ResultsDataService) {

  }

requestResults(phrase: string) {
  this.resultsDataService
  .getResults(phrase)
  .subscribe(
    (results) => {
      if (results && results.query) {
        this.results = results.query.pages.map(record => {
          return {
            url: record.fullurl,
            description: record.extract || 'no description',
            img: record.thumbnail ? record.thumbnail.source : ''
          }
        });
      } else {
        this.results = [];
      }
    }
  );
}
    
}
