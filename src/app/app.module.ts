import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';

import { AppComponent } from './app.component';
import { ResultComponent } from './components/result/result.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { ResultListComponent } from './components/result-list/result-list.component';
import { SearchComponent } from './components/search/search.component';

import { ResultsDataService } from './services/results-data.service';

@NgModule({
  declarations: [
    AppComponent,
    ResultComponent,
    HeaderComponent,
    FooterComponent,
    ResultListComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    InputTextModule,
    ButtonModule
  ],
  providers: [ResultsDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
