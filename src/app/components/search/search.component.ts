import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  phrase: string = '';
  error: boolean = false;

  @Output()
  search: EventEmitter<string> = new EventEmitter();

  constructor() { }

  emitPhrase() {
    if (this.phrase.trim() !== '') {
      this.error = false;
      this.search.emit(this.phrase.trim());
    } else {
      this.error = true;
    }
  }

}
