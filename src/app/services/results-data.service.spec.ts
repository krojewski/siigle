import { TestBed, inject } from '@angular/core/testing';

import { ResultsDataService } from './results-data.service';

describe('ResultsDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResultsDataService]
    });
  });

  it('should be created', inject([ResultsDataService], (service: ResultsDataService) => {
    expect(service).toBeTruthy();
  }));
});
