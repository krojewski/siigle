import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

import { Response, Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const WIKI_URL = environment.wikiUrl;

@Injectable()
export class ResultsDataService {

  constructor(private jsonp: Jsonp) {}

  public getResults(phrase: string): Observable<any> {
    return this.jsonp
  .request(`${WIKI_URL}?action=query&format=json&formatversion=2&generator=prefixsearch&gpslimit=10&prop=pageimages|pageterms|info|extracts&exintro=true&exsentences=2&explaintext=true&inprop=url&piprop=thumbnail&pithumbsize=250&pilimit=10&redirects=1&wbptterms=description&callback=JSONP_CALLBACK&gpssearch=${phrase}`)

  .map(response => {
    const results = response.json();
    return results;
  })
  .catch(this.handleError);
}

  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }
  
  
}
